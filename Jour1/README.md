
#Linux / Shell
## cut

cut est un outil qui permet de découper des chaines de caractères selon un délémiteur.

On spécifie le délimiteur avec l'option -d ':' (où ':' peut être n'importe quel autre caractère)

On choisit la colonne à garder grâce à l'option -f suivi d'un ou plusieurs chiffres

Exemples :

On peut choisir une seule colonne à garder :

```sh
echo "john:doe:22:Toulouse" | cut -d':' -f 1
john
```

```sh
echo "john,doe,22,Toulouse" | cut -d',' -f 3
22
```
Si on veut garder la colonne 1 et la colonne 4 :

```sh
echo "john:doe:22:Toulouse" | cut -d':' -f 1,4
john:Toulouse
```

Si on veut garder depuis la colonne 2 jusqu'à la colonne 4 :
```sh
echo "john:doe:22:Toulouse" | cut -d':' -f 2-4
doe:22:Toulouse
```

Le plus important :

```sh
man cut
```

## wc

wc permet de compter le nombre de lignes, de mots, de caractères ou d'octets d'un flux (fichier, commande pipée etc.)

Exemples :

On peut compter le nombre de lignes grâce à '-l' :
```sh
wc -l fichier
56
```

On peut compter le nombre de mots grâce à '-w' :
```sh
wc -w fichier
123
```

On peut compter le nombre de caractères grâce à l'option '-c' :
```sh
wc -c fichier
3029
```

wc peut aussi s'utiliser sur un flux grâce à un pipe :
```sh
echo "toto toto toto2 toto3" | wc -w
4
```

Et toujours en cas de doute :
```sh
man cut
```

## grep

grep permet de filtrer les lignes d'un flux ou d'un fichier selon un motif fourni :

Attention, grep est sensible à la casse par défaut ! Par contre l'option '-i' permet d'ignorer la casse.

Je veux afficher seulement les lignes contenant le mot "false" :
```sh
grep false /etc/passwd

systemd-timesync:x:100:103:systemd Time Synchronization,,,:/run/systemd:/bin/false
systemd-network:x:101:104:systemd Network Management,,,:/run/systemd/netif:/bin/false
systemd-resolve:x:102:105:systemd Resolver,,,:/run/systemd/resolve:/bin/false
systemd-bus-proxy:x:103:106:systemd Bus Proxy,,,:/run/systemd:/bin/false
messagebus:x:104:109::/var/run/dbus:/bin/false
[...]
```

On peut inverser la sélection en utilisant l'option '-v' de grep, par exemple si je veux afficher tout le fichier sauf les lignes contenant le mot "false" :

```sh
grep -v false /etc/passwd

root:x:0:0:root:/root:/bin/bash
daemon:x:1:1:daemon:/usr/sbin:/usr/sbin/nologin
bin:x:2:2:bin:/bin:/usr/sbin/nologin
sys:x:3:3:sys:/dev:/usr/sbin/nologin
sync:x:4:65534:sync:/bin:/bin/sync
[...]
```

Je peux rechercher plusieurs motifs à la fois dans un fichier grâce à -E et '|'. Par exemple si je veux garder les lignes contenant "false" ou "nologin" :

```sh
grep -E 'false|nologin' /etc/passwd

gnats:x:41:41:Gnats Bug-Reporting System (admin):/var/lib/gnats:/usr/sbin/nologin
nobody:x:65534:65534:nobody:/nonexistent:/usr/sbin/nologin
systemd-timesync:x:100:103:systemd Time Synchronization,,,:/run/systemd:/bin/false
systemd-network:x:101:104:systemd Network Management,,,:/run/systemd/netif:/bin/false
systemd-resolve:x:102:105:systemd Resolver,,,:/run/systemd/resolve:/bin/false
[...]
```

Oh et un dernier conseil utile :
```sh
man grep
```

# Git

Pour cloner (récupérer un dépôt distant vers la machine locale) :
```sh
git clone git@gitlab.com:Aithios/Simplon_Devops.git (remplacer l'URL par le repo Git voulu)
```

On ajoute / modifie son fichier (éditeur au choix) :
```sh
vim fichier
nano fichier
gedit fichier
```

On ajoute le fichier qu'on vient d'ajouter ou de modifier aux fichiers qui vont être "commit" :
```sh
git add fichier
```

On commit les modifications, c'est-à-dire qu'on dit à git qu'on est prêt à les envoyer :

Veillez à mettre un message de commit qui permet de savoir ce que vous avez modifié sur le dépôt.

```sh
git commit -m "Modifying file members.js to add a new field"
```

On envoie le tout vers le repo Git en question :
```sh
git push
```

À chaque étape on peut vérifier le bon fonctionnement du process en tapant :
```sh
git status
```
