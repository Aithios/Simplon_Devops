# Exemples concrets d'utilisation de grep / cut / sort ...

Dans ce dossier, vous trouverez deux fichiers pour vous entrainer :
- access.log qui pèse 193K
- access.log.full qui pèse 2M et qui contient beaucoup plus de lignes que access.log


On a donc le fichier access.log qui contient 1000 lignes d'accès HTTP à un serveur web en particulier.
Voici un exemple de sa structure :

```sh
109.169.248.247 - - [12/Dec/2015:18:25:11 +0100] "GET /administrator/ HTTP/1.1" 200 4263 "-" "Mozilla/5.0 (Windows NT 6.0; rv:34.0) Gecko/20100101 Firefox/34.0" "-"
109.169.248.247 - - [12/Dec/2015:18:25:11 +0100] "POST /administrator/index.php HTTP/1.1" 200 4494 "http://almhuette-raith.at/administrator/" "Mozilla/5.0 (Windows NT 6.0; rv:34.0) Gecko/20100101 Firefox/34.0" "-"
46.72.177.4 - - [12/Dec/2015:18:31:08 +0100] "GET /administrator/ HTTP/1.1" 200 4263 "-" "Mozilla/5.0 (Windows NT 6.0; rv:34.0) Gecko/20100101 Firefox/34.0" "-"
```

Imaginons que nous voulons savoir quelle adresse IP a fait le plus de requêtes sur ce serveur web :

```sh
cat access.log | awk '{print $1}' | sort | uniq -c | sort -r | head
     37 191.182.199.16
     31 188.45.108.168
     18 37.1.206.196
     17 83.167.113.100
     16 109.106.143.62
     12 188.191.175.166
     10 87.255.246.50
     10 178.204.13.65
     10 177.98.255.56
      8 93.76.246.140

```
Ici, nous avons utilisé la commande awk pour n'afficher que la première colonne ($1), ensuite via sort on trie le résultat. Grâce à uniq, nous allons pouvoir filtrer pour n'afficher que les résultats uniques, et grâce à l'option '-c' on peut compter ces résultats. On finit par trier les résultats dans l'ordre inverse grâce à sort avec l'option -r (reverse). Enfin grâce à head nous récupérons seulement les 10 premiers résultats.


On peut imaginer la même chose mais sur les URL appelées en changeant tout simplement le paramètre de la colonne que l'on passe à awk. On remplace donc $1 par $7 :

```sh
cat access.log | awk '{print $7}' | sort | uniq -c | sort -r | head
    437 /administrator/index.php
    423 /administrator/
     31 /apache-log/access.log
     23 /
     12 /robots.txt
      3 /templates/_system/css/general.css
      3 /images/stories/slideshow/almhuette_raith_07.jpg
      3 /images/stories/slideshow/almhuette_raith_06.jpg
      3 /images/stories/slideshow/almhuette_raith_05.jpg
      3 /images/stories/slideshow/almhuette_raith_04.jpg
```

N'hésitez pas à jouer avec les différentes colonnes affichables avec awk et les options des différentes commandes !

# awk

Soit un fichier "marks" qui contient les lignes suivantes :

```
2) Rahul    Maths     90
3) Shyam    Biology   87
4) Kedar    English   85
5) Hari     History   89

```

Si on veut afficher seulement la deuxième colonne :

```
awk '{print $2}'
Rahul
Shyam
Kedar
Hari
```

Si on veut afficher les notes supérieures à 90, puis afficher "nom" a eu "note" en "matière" :

```
awk '$4 >= 90' marks| awk '{print $2 " a eu " $4 " en " $3}'
Rahul a eu 90 en Maths
```

Si on veut afficher les notes inférieures ou égales à 89, puis afficher la même phrase qu'au-dessus :

```
awk '$4 <= 89' marks| awk '{print $2 " a eu " $4 " en " $3}'
```
