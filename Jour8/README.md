# Jour 8

## Rappel des consignes du Jour 7 

- Mettre en place un monitoring avec des graphs (Munin conseillé, sinon grafana, collectd ...)
- Mettre en place des graphs sur des métriques de votre serveur (CPU, RAM, Disk, réseau ...)
- Bonus : Mettre en place des graphs avec métriques sur vos applications (nombre de visites, base de données, temps de réponse ...)
- Bonus2 : Mettre en place un système d'alerte (Nagios ?) en cas de défaillance de votre application

## Consignes du Jour 8 :

- Se renseiger sur ce qu'est un backup
- Écrire un script qui va créer un backup de votre service (attention, pas de backup du serveur entier, juste du service hébergé et des outils associés)
- Par exemple : backup du répertoire /var/www/gitlab + backup de la base de données MySQL dans le répertoire /backup.
- Envoyer vos backups vers un serveur distant (la VM AWS de votre voisin par exemple)
- Lancer le script de backup automatiquement tous les jours à 12h
- Écrire une doc qui explique les étapes pour _restaurer_ votre service
- Bonus : Possibilité de garder 3 jours de backups (ex : 'gitlab_backup_09072018.tar.gz' , 'gitlab_backup_10072018.tar.gz' , 'gitlab_backup_11072018.tar.gz' etc.)
  - Si vous avez besoin de plus d'espace disque sur votre serveur pour vos backups, demandez ! :)

## Liens et ressources utiles :

- https://www.reddit.com/r/sysadmin/wiki/backups
- MySQLdump : https://mariadb.com/kb/en/library/making-backups-with-mysqldump/
- pg_dump : https://www.postgresql.org/docs/9.5/static/backup-dump.html
- tar : https://www.zyxware.com/articles/2009/02/26/how-to-create-and-extract-a-tar-gz-archive-using-command-line
- cron : https://doc.ubuntu-fr.org/cron
