# Les paquets

## apt

```
to be continued
```

## dpkg

```
to be continued
```


# Installation et hébérgement d'un logiciel libre

## Consignes

- Envoyer votre clé SSH **publique** accompagnée de votre prénom et du numéro du groupe à yanis.boucherit@gmail.com
- Choisir un service parmi la liste ci-dessous. Toutes les suggestions sont les bienvenues (à valider avec moi si vous avez une idée de service)
- Se connecter à la VM fournie et héberger le service
- Tout au long du process :
  - Tenir une documentation à jour sur votre Gitlab décrivant et **expliquant** les étapes effectuées
  - Mettre les références utilisées (sites web, vidéos, docs ...) en fin de documentation

## Infos de connexion et groupes :

- Groupe 1 : Gitlab : Amaury, Haingo, Jeanne

=> ssh : simplon@52.47.164.77
=> IP Publique : 52.47.164.77

- Groupe 2 : FusionForge : Alaric, Laurent, Nabila

=> ssh simplon@ec2-52-47-164-113.eu-west-3.compute.amazonaws.com
=> IP Publique : 52.47.164.113

- Groupe 3 : Mattermost : Cedric, Ethan, Louai, Rado

=> ssh simplon@ec2-35-180-97-126.eu-west-3.compute.amazonaws.com
=> Ip Publique : 35.180.97.126


- VMs Individuelles :
  
  - Jeanne :
    simplon@35.180.100.244
  - Nabila :
    simplon@35.180.116.214
  - Amaury :
    simplon@52.47.155.207
  - Haingo :
    simplon@52.47.191.26
  - Alaric :
    simplon@52.47.188.207
  - Laurent :
    simplon@35.180.97.116
  - Cedric :
    simplon@35.180.69.193
  - Ethan :
    simplon@52.47.162.95
  - Louai :
    simplon@35.180.109.156
  - Rado :
    simplon@35.180.66.88


## Liste des logiciels à choisir

- Bugzilla
- DokuWiki
- Drupal
- Etherpad Lite
- Gitlab
- Joomla
- Mattermost
- MediaWiki
- Moodle
- osTicket
- OwnCloud
- phpBB
- PunBB
- Redmine
- Trac
- Twiki
- WordPress
- Zen Cart

## Doc

- https://doc.ubuntu-fr.org/lamp
- https://doc.ubuntu-fr.org/tutoriel/virtualhosts_avec_apache2
- https://doc.ubuntu-fr.org/mysql
- https://doc.ubuntu-fr.org/php
- https://doc.ubuntu-fr.org/nginx
- https://www.techrepublic.com/article/how-to-start-stop-and-restart-services-in-linux/
- https://degooglisons-internet.org/liste/
