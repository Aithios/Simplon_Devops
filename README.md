Git de la promo Simplon / Gekko pour la formation DevOps à Toulouse !

Have fun :)

## Outils hébergés par vous-même :)

- Mattermost : Responsables : Rado, Ethan, Cedric, Louai
  - https://mattermost.geeked.fr

- Dokuwiki : Responsables : Jeanne, Amaury, Haingo
  - https://dokuwiki.geeked.fr

- Moodle : Responsables : Nabila, Laurent, Alaric
  - https://moodle.geeked.fr

## De l'importance de vérifier ses backups
- https://about.gitlab.com/2017/02/10/postmortem-of-database-outage-of-january-31/

# Debian

https://debian-handbook.info/download/fr-FR/stable/debian-handbook.pdf

# Veille

L'idée ici ça n'est pas de "binge reader" tous les liens ci-dessous, mais plutôt
de prendre l'habitude dès que vous avez un peu de temps libre, de surfer et de 
se tenir au courant de ce qui sort, des nouvelles tendances, des sujets de fond 
etc.

- https://www.reddit.com/r/devops/
- https://news.ycombinator.com/
- https://hackernoon.com/
- http://www.commitstrip.com/fr/
- https://www.devopsweekly.com/

# DevOps ?

Quelques ressources pour comprendre l'idée derrière DevOps et un peu de food for
thought :)

- Road to DevOps : https://github.com/kamranahmedse/developer-roadmap#-devops-roadmap
- https://www.youtube.com/watch?v=_I94-tJlovg
- https://drive.google.com/drive/u/0/folders/1zJwctmPui3zRY0Ni1gUtRpzi4qxeGs8G
- https://www.manning.com/books/securing-devops
- https://medium.com/@mattklein123/the-human-scalability-of-devops-e36c37d3db6a


# Semaine 2

- Linux
- Shell
- Bash

## [Jour 1](Jour1/)

Points abordés : 
- Découverte de Linux via une petite intro
- Découverte de son environnement de travail
- Découverte de Linux via des exercices pratiques
- Mise en commun / correction ensemble des exercices

Liens et ressources utiles :

- http://lea-linux.org/documentations/Intro-linux
- https://openclassrooms.com/courses/reprenez-le-controle-a-l-aide-de-linux
- http://www.linuxcommand.org/
- https://ryanstutorials.net/linuxtutorial/
- https://alexpetralia.com/posts/2017/6/26/learning-linux-bash-to-get-things-done

## [Jour 2](Jour2/)

- Explications et cas concrets sur l'utilisation de grep / cut / sort (voir README du J2)
- Challenges : https://cmdchallenge.com/
- Fin de la correction des exercices du J1
- Lightning-talk par Ethan sur son script de nettoyage d'Ubuntu (https://gitlab.com/Ethik69/UbuntuCleaner)
- Suite des challenges et correction tous ensemble

## [Jour 3](Jour3/)

- Suite des cmdchallenges
- Présentation du Shell Scripting et de la commande "read"
- Exercice d'introduction à read / aux boucles (j3/ex1)
- Lightning talk par Cedric sur Scrapy (https://scrapy.org/), une bibliothèque python pour faire du web-scraping.
- Correction de l'exercice sur read et les boucles
- Début du projet BashDB en groupe

## [Jour 4](Jour4/)

- Suite du projet BashDB en groupe
- Lightning talk par Laurent sur son script d'install de Guacamole (https://gitlab.com/Metal65/Script_Guacamole)
- Suite du projet BashDB


## [Jour 5](Jour5/)

- Challenges bandit
- Suite et fin du projet BashDB
- Lightning talk par Ethan sur l'IA (machine learning, réseaux neuronaux ...) https://gitlab.com/Ethik69/Neural-Network / Game of Life (https://gitlab.com/Ethik69/Game_of_life)
- Restitutions / démo BashDB
- France - Uruguay (2 - 0) !!

# Semaine 3

- apt / dpkg
- lamp
- apache
- nginx
- let's encrypt
- monitoring

## [Jour 6](Jour6/)

- Presentation d'apt et de dpkg (voir le .odp dans le Jour6)
- Projet d'hébergement d'un logiciel libre en groupe (voir README.md du Jour6)
- Lightning talk sur VMWare par Rado et Laurent
- Lightning talk Ansible + Provisioning EC2 par Yanis
- Suite de l'hébergement des services

## [Jour 7](Jour7/)

- Suite de l'hébergement des services
- Lightning talk par Ethan sur les aliases Bash (https://doc.ubuntu-fr.org/alias) et sur Conky (https://doc.ubuntu-fr.org/conky)
- Présentation du monitoring et de la place du monitoring dans un process DevOps (voir .odp dans le Jour7)
- Suite de l'hébergement et ajout de monitoring

## [Jour 8](Jour8/)

- Suite de l'hébergement des services
- Courte présentation notions de sécurité et de fichiers sur Github
- Lightning talk sur les systèmes de bases de données distribuées : par Louai
- Monitoring des services
- Backups des services

## [Jour 9](Jour9/)

- Suite de l'hébergement des services
- Monitoring des services
- Backups des services
- Lightning talk par Haingo sur les cron (https://help.ubuntu.com/community/CronHowto)
- Préparation des réstitutions pour demain

## [Jour 10](Jour10/)

- Réstitutions des projets de la semaine
- Kata / CodingDojo sur l'exercice "Hangman" en Python
- Challenges (cmdchallenge, Bandit, git (https://gitexercises.fracz.com/ ))

# Semaine 4

- Git
- Ansible
- Terraform
- Docker

## [Jour 11](Jour11/)

- Présentation Git
- Exercices sur Git
- Lightning talk par Nabila sur les backups
- Suite des exos sur Git
- 1er tour de correction des exos

## [Jour 12](Jour12/)

- Correction ensemble des exercices Git de la veille
- Présentation d'Ansible
- Présentation du projet en groupe d'automatisation d'install d'un serveur + projet
- Présentation de GitKraken par Ethan (https://www.gitkraken.com/)
- Travail en groupe sur le projet d'automatisation

## [Jour 13](Jour13/)

- Suite du projet d'automatisation en groupe
- Lightning talk par Cédric sur Tableau et le BigData (https://www.tableau.com/fr-fr) et sur Anaconda (https://anaconda.org/anaconda)
- Suite du projet d'automatisation en groupe

## [Jour 14](Jour14/)

- Suite du projet d'automatisation en groupe
- Lightning talk par Laurent sur sa manière de découper des réseaux
- Suite du projet d'automatisation
- Intervention de Karim Boukercha (https://twitter.com/karim_boukercha) pour nous raconter son parcours de consultant ...

## [Jour 15](Jour15/)

- Suite du projet d'automatisation en groupe
- Restitutions du projet
- Exercice 'affiche_z' (voir Jour15)
- Coding Dojo sur le "plus ou moins" en Python
- Bilan
