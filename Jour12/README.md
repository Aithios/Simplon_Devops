# Consignes

Projet à réaliser en groupe jusqu'à vendredi.

### 1ère étape

- Configurez Ansible avec un host qui sera votre serveur Amazon (voir plus bas)
- Utilisez Ansible pour installer le paquet htop sur vos serveurs
- Relancez Ansible une deuxième fois sur le même serveur. Que constatez-vous ?

### Déploiement automatique d'une instance de Wordpress ou de votre projet

Vous allez héberger un blog sous Wordpress ou un projet de votre choix (pré-requis : BDD + "interface web").

Par groupe, vous vous occuperez de l'héberger sur une VM fournie, mais attention :

L'installation / le déploiement se fera via Ansible :
- Toute l'installation (au moins jusqu'à l'install "navigateur") doit être automatique. Il faudra veiller à automatiser :
  - L'installation et la configuration du serveur web (apache2, nginx ...) avec PHP7
  - Le téléchargement et la décompression de wordpress ou du projet dans son docroot
  - L'installation et la configuration de la base de données (utilisateurs + création de la BDD + droits ...)
  - Le monitoring (Munin est un bon choix, rapide à mettre en place)


- Pour ceux qui ont du temps devant eux :
  - Automatiser de A à Z l'installation de Wordpress.

    Exemple :
   ```
    ansible-playbook --extra-vars "blog_name=TotoBlog dbuser=toto dbpassword=123456" install.yml
    ```

    - Une commande similaire à celle-ci me permettra d'accéder à une instance de Wordpress prête à être utilisée avec le nom de domaine {{ blog_name }}.geeked.fr et une base de données configurée avec l'user toto et le mot de passe 123456.


- Veillez à gérer la collaboration dans votre groupe via un repo Git en utilisant les branches etc.

- N'hésitez pas à demander si vous avez besoin d'un sous domaine (xxxx.geeked.fr)

## Instances

- Pour plus de simplicité avec Ansible, vous pouvez utiliser "sudo" sans mot de passe.

- Vos instances personnelles sont toujours disponibles pour vos tests (voir README du Jour6).

- Par groupe vous avez une instance "de prod" :

- Groupe 1 : Ethan, Louai, Amaury
  - ssh simplon@35.180.32.13

- Groupe 2 : Cedric, Nabila, Haingo
  - ssh simplon@35.180.58.77

- Groupe 3 : Jeanne, Alaric, Rado, Laurent
  - ssh simplon@35.180.103.59

## Liens utiles

### Infrastructure as Code : généralités

- https://www.lemagit.fr/definition/Infrastructure-as-a-Code
- https://blog.octo.com/bonne-nouvelle-linfra-as-code-cest-du-code/

### Ansible
- https://docs.ansible.com/
- https://docs.ansible.com/ansible/devel/user_guide/intro_getting_started.html
- https://scotch.io/tutorials/getting-started-with-ansible


