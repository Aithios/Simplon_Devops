## Git

Commencer les challenges sur le site suivant :

- https://gitexercises.fracz.com/

N'oubliez pas que ça n'est pas une course, ni un exercice totalement individuel. Vos camarades sont là pour vous aider si vous êtes bloqués, donc n'hésitez pas à discuter entre vous.

### Ressources utiles pour Git :

- https://medium.com/@OVHUXLabs/la-puissance-des-workflows-git-12e195cafe44
- https://openclassrooms.com/fr/courses/2342361-gerez-votre-code-avec-git-et-github/2433691-creez-des-branches
- https://blog.eleven-labs.com/fr/git-rebase/
- https://learngitbranching.js.org/?NODEMO
- https://try.github.io/

