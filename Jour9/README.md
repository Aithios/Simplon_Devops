# Consignes

## Monitoring

- Finir la mise en place de votre outil de monitoring [voir Jour 8](Jour8/)

## Backups

- Finir la mise en place des backups [voir Jour 8](Jour8/)

## Git

Commencer les challenges sur le site suivant :

- https://gitexercises.fracz.com/

N'oubliez pas que ça n'est pas une course, ni un exercice totalement individuel. Vos camarades sont là pour vous aider si vous êtes bloqués, donc n'hésitez pas à discuter entre vous.

### Ressources utiles pour Git :

- https://medium.com/@OVHUXLabs/la-puissance-des-workflows-git-12e195cafe44
- https://openclassrooms.com/fr/courses/2342361-gerez-votre-code-avec-git-et-github/2433691-creez-des-branches
- https://blog.eleven-labs.com/fr/git-rebase/
- https://learngitbranching.js.org/?NODEMO
- https://try.github.io/

## Réstitutions

- Durant le jour 10, chaque groupe va passer présenter son projet

- Pour le groupe qui passe :

- Préparez en groupe votre réstitution de demain :
  - 1ère partie de ~20 minutes sur la technique :
    - Présentation et démo de l'outil mis en place
    - Explication et review rapide de la doc
    - Les difficultés techniques rencontrées
    - Les facilités / les points où vous vous sentez à l'aise
    - Démo du monitoring
    - Démo du backup

  - 2ème partie de ~10 minutes sur le groupe et l'organisation :
    - Comment s'est organisé le groupe ? (Qui a fait quoi ?)
    - Les difficultés rencontrées dans le travail en groupe
    - Avec l'experience acquise dans la semaine, si vous pouviez refaire ce projet depuis le début, qu'auriez-vous changé dans l'organisation du groupe ?

- Veillez à ce que tous les membres du groupe aient un temps de parole égal
- Tous les membres du groupes doivent être au courant de tout ce qui a été fait et mis en place


- Pour les auditeurs :
  - Pendant la présentation des groupes, n'hésitez pas à poser des questions
  - Intervenez si vous voyez quelque chose que vous ne comprenez pas ou quelque chose qui vous semble "bizarre"
  - Mais restez bienveillants les uns envers les autres

### Bon courage !
