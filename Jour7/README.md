# Monitoring

Voir le fichier .odp pour les slides.

## Consignes

- Mettre en place un monitoring avec des graphs (Munin conseillé, sinon grafana, collectd ...)
- Mettre en place des graphs sur des métriques de votre serveur (CPU, RAM, Disk, réseau ...)
- Bonus : Mettre en place des graphs avec métriques sur vos applications (nombre de visites, base de données, temps de réponse ...)
- Bonus2 : Mettre en place un système d'alerte (Nagios ?) en cas de défaillance de votre application

## Le monitoring, comment font les autres ?

- How google monitors things : https://landing.google.com/sre/book/chapters/monitoring-distributed-systems.html
- Uber tech stack (partie monitoring / logs) : https://eng.uber.com/tech-stack-part-one/
- A day in the life of Facebook operations : https://www.youtube.com/watch?v=T-Xr_PJdNmQ
- Zen and The Art of Monitoring : https://www.scalyr.com/community/guides/zen-and-the-art-of-system-monitoring
- 10 monitoring talks every developer should watch : https://techbeacon.com/10-monitoring-talks-every-developer-should-watch


## Outils

- Munin (pour les graphs) : https://doc.ubuntu-fr.org/munin
- Nagios (pour l'alerting) : https://doc.ubuntu-fr.org/nagios
- Grafana (graphs) : http://docs.grafana.org/installation/debian/
- Et un million d'autre outils que vous pouvez découvrir via Google :)
