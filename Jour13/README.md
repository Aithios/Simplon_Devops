## Liens utiles

### Infrastructure as Code : généralités

- https://hackernoon.com/infrastructure-as-code-tutorial-e0353b530527
- https://www.lemagit.fr/definition/Infrastructure-as-a-Code
- https://blog.octo.com/bonne-nouvelle-linfra-as-code-cest-du-code/

### Ansible
- https://docs.ansible.com/
- https://docs.ansible.com/ansible/devel/user_guide/intro_getting_started.html
- https://scotch.io/tutorials/getting-started-with-ansible

